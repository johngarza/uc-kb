'use strict';

var acts = {};
var foods = {};
var schedules = {};
var features = {};

//var kb_blue = '#324b77';
//var kb_blue_light = '#293a65';
var kb_blue_dark = '#0a2965';
var kb_orange = '#e15f01';
//var kb_orange_light = '#f3e2d0';
//var kb_orange_dark = '#c4582b';
var kb_white = '#fff';
var kb_outline = '#aaa';

var activityMarkerOptions = {
  radius: 10,
  fillColor: kb_blue_dark,
  color: kb_outline,
  weight: 1.5,
  opacity: 1,
  fillOpacity: 0.8
};

var scheduleMarkerOptions = {
  radius: 10,
  fillColor: kb_white,
  color: kb_outline,
  weight: 0,
  opacity: 0.55,
  fillOpacity: 0.55
};

var foodMarkerOptions = {
  radius: 10,
  fillColor: kb_orange,
  color: kb_outline,
  weight: 1.5,
  opacity: 1,
  fillOpacity: 0.8
};

var popupOptions = {
  maxWidth : 280,
  autoPanPaddingTopLeft:L.point(0,80),
  autoPanPaddingBottomRight:L.point(0,80)
};

var featureTmpl = $('#ActivityPopup').html();

var app = (function(document, $) {
  /*
    var docElem = document.documentElement,
		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
  */
		var _init = function() {
			$(document).foundation();
			//_userAgentInit();
      acts = (function() {
              var acts = null;
              $.ajax({
                  'async': false,
                  'global': false,
                  'url': 'geojson/activities.js',
                  'dataType': 'json',
                  'success': function (data) {
                      acts = data;
                  }
              });
              return acts;
          })();
      foods = (function() {
        var foods = null;
        $.ajax({'async': false,
          'global': false,
          'url': 'geojson/food.js',
          'dataType': 'json',
          'success': function (data) {
            foods = data;
          }
        });
        return foods;
      })();
      schedules = (function() {
        var schedules = null;
        $.ajax({'async': false,
          'global': false,
          'url': 'geojson/schedule.js',
          'dataType': 'json',
          'success': function (data) {
            schedules = data;
          }
        });
        return schedules;
      })();
		};

	return {
		init: _init
	};

})(document, jQuery);

(function() {
	app.init();
})();

//map view bounds definition
//-98.6326,29.5765,-98.6064,29.5912
var northEast = L.latLng(29.5912, -98.6329);
var southWest = L.latLng(29.5765, -98.6064);
var bounds = L.latLngBounds(northEast, southWest);
var zoomControl = new L.Control.Zoom({ position: 'bottomright' });

var map = L.map('map', { zoomControl: false});
var tileLayer = L.tileLayer('tiles/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
				'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
  maxZoom: 19,
  minZoom: 16
});

var locateControl = L.control.locate({
    position: 'bottomright',
    drawCircle: true,
    markerClass: L.circleMarker,
    follow: true,
    remainActive: true,
    setView: false,
    keepCurrentZoomLevel: true, 
    icon: 'icon-location',
    iconLoading: 'icon-spinner animate-spin',
    metric: false,
    onLocationError: function(err) {
      console.log('location error encountered');
      console.log(err);
    },
    onLocationOutsideMapBounds:  function(context) { 
      // called when outside map boundaries
      console.log('you are out of bounds!');
      console.log(context);
    },
    showPopup: false,
    strings: {
        title: 'Show me where I am',  // title of the locate control
        popup: 'You are within {distance} {unit} from this point',  // text to appear if user clicks on circle
        outsideMapBoundsMsg: 'You seem located outside the boundaries of the map' // default message for onLocationOutsideMapBounds
    }
});

var legend = L.control({position: 'bottomleft'});

legend.onAdd = function () {
    var div = L.DomUtil.create('div', 'info legend');
    div.innerHTML += '<i style="background:' + kb_orange + '"></i>';
    div.innerHTML += 'Food<br/>';
    div.innerHTML += '<i style="background:' + kb_blue_dark + '"></i>';
    div.innerHTML += 'Activity<br/>';
    div.innerHTML += '<i style="background:' + kb_white + '"></i>';
    div.innerHTML += 'Event<br/>';
    return div;
};

tileLayer.addTo(map);
map.setMaxBounds(bounds);
map.setView([29.5835,-98.6202], 18);
zoomControl.addTo(map);
locateControl.addTo(map);
legend.addTo(map);

function onEachFeature(feature, layer) {
  //prepare the model for our html content template
  var model = {
    'title': feature.properties.title,
    'location': feature.properties.location,
    'description': feature.properties.description
  };
  var featureContent = _.template(featureTmpl, model);
  layer.bindPopup(featureContent, popupOptions);
}

var geoFeatures = L.geoJson(null, {
    pointToLayer: function (feature, latlng) {
      var markerOptions = activityMarkerOptions;
      var featureType = feature.properties.type;
      var marker = null;
      if (featureType === 'food') {
        markerOptions = foodMarkerOptions;
        marker = L.circleMarker(latlng, markerOptions);
        //foodLayer.addLayer(marker);
      }
      if (featureType === 'schedule') {
        markerOptions = scheduleMarkerOptions;
        marker = L.circle(latlng, 9, markerOptions);
        //scheduleLayer.addLayer(marker);
      }
      if (featureType === 'activity') {
        markerOptions = activityMarkerOptions;
        marker = L.circleMarker(latlng, markerOptions);
        //actLayer.addLayer(marker);
      }
      features[feature.properties.id] = marker;
      return marker;
    },
    onEachFeature: onEachFeature
});

geoFeatures.addTo(map);
geoFeatures.addData(acts);
geoFeatures.addData(foods);
geoFeatures.addData(schedules);

map.on('popupopen', function(e) {
  var trac_id = 'location-' + e.popup._source.feature.properties.id;
  //console.log(trac_id);
  ga('send', 'event', trac_id, 'clicked');
});

var KickbackRoutes = Backbone.Router.extend({
  routes: {'show/:id': 'show'},  
  show: function(id) { 
    features[id].openPopup();
  }
});

$(function(){
	window.app = new KickbackRoutes();
	Backbone.history.start();	
});
