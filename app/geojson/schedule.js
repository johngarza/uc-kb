{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "id": 100,
        "name": "Kickback Stage",
        "title": "Kickback Stage",
        "location": "UC Paseo - Ski Lodge Patio",
        "type": "schedule",
        "description": "<ul><li><b>4pm</b> - Welcome featuring Spirit of San Antonio Band, Rowdy, and UTSA Cheerleaders</li><li><b>4pm-6pm</b> - Live music from <em>Atlantis</em></li><li><b>5pm</b> - Student Performance</li></ul>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62024962902069,
          29.58355984045559
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 102,
        "name": "UCinema Movie",
        "title": "UCinema Presents: <em>Maleficent</em>",
        "location": "Retama Auditorium - UC 2nd Floor",
        "type": "schedule",
        "description": "<ul><li><b>6pm</b> - UCinema presents: <em>Maleficent</em></li></ul>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62025499343872,
          29.584149973169236
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 101,
        "name": "Cupcakes!",
        "title": "Cupcake Celebration",
        "location": "UC North sliding doors near C3",
        "type": "schedule",
        "description": "<ul><li><b>5:30PM</b> - Cupcake Celebration</li></ul>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61973464488983,
          29.583723118626782
        ]
      }
    }
  ]
}