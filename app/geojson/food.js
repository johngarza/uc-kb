{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "id": 1,
        "name": "Facebook Likes",
        "title": "HA Candy for Facebook Likes",
        "location": "Honors Alliance Table - UC North 1st Floor",
        "type": "food",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62009137868881,
          29.583895726691964
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 0,
        "name": "Free Candy!",
        "title": "Free Candy!",
        "location": "Special Events Center - Paseo HUC 1.224",
        "type": "food",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61994922161102,
          29.58350852440438
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 3,
        "name": "Chicken",
        "title": "Chicken on a Stick",
        "location": "Ski Lodge Patio - Paseo",
        "type": "food",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62042665481566,
          29.583443213028737
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 4,
        "name": "Cones and Dogs",
        "title": "Cones and Dogs",
        "location": "HUC Subway - Paseo",
        "type": "food",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62028986215591,
          29.583324252914483
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 10,
        "name": "Paletas",
        "title": "Paletas",
        "location": "Student Center for Community Engagement and Inclusion Table - UC North 1st Floor",
        "type": "food",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61994385719298,
          29.583895726691964
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 15,
        "name": "Slushies",
        "title": "Slushies in the Student Activites Office",
        "location": "Office of Student Activities - HUC 1.210",
        "type": "food",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62036496400833,
          29.583151643871933
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 16,
        "name": "S'moretopia",
        "title": "S'moretopia",
        "location": "HUC Storefronts (SGA, Roadrunner Productions, Fraternity/Sorority Life offices)",
        "type": "food",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.6200886964798,
          29.583431550278625
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 22,
        "name": "Popcorn",
        "title": "Popcorn",
        "location": "UC North Inside Sliding Doors - Ski Lodge",
        "type": "food",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.6205580830574,
          29.583422220077576
        ]
      }
    }
  ]
}