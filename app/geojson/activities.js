{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "id": 2,
        "name": "Caricatures",
        "title": "Caricatures",
        "location": "Near UC North sliding doors",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61999481916428,
          29.58381175523767
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 5,
        "name": "Human Board Game",
        "title": "Human Board Game",
        "location": "Inside VOICES Office - HUC 1.216",
        "type": "activity",
        "description": "Participate in a lifesized board game where you can be the game pieces!"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62009674310684,
          29.58335690864608
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 6,
        "name": "Jeopardy",
        "title": "Jeopardy Game",
        "location": "Princeton Review - UC North 1st Floor Corridor",
        "type": "activity",
        "description": "Play jeopardy with Princeton Review and earn prizes!"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62015575170517,
          29.58397503300132
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 7,
        "name": "Lunch with the Dean",
        "title": "Lunch with the Dean Content",
        "location": "Office of Student Life - UC North 1st Floor Corridor",
        "type": "activity",
        "description": "Enter to win the special opportunity to have lunch with the Dean of Students!"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61997067928314,
          29.58395170762267
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 8,
        "name": "Memory Game",
        "title": "Memory Game",
        "location": "Student Leadership Center Office - Paseo",
        "type": "activity",
        "description": "Test your memory skills, learn about the UTSA Student Leadership Center and win great prizes!"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62008064985275,
          29.58373711388631
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 9,
        "name": "Obstacle Course",
        "title": "Inflatable Obstacle Course",
        "location": "UC Lawn",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61982315778731,
          29.58303035085466
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 11,
        "name": "Photo Booth",
        "title": "Kickback Couch Photo Booth",
        "location": "Rowdy Lawn",
        "type": "activity",
        "description": "Get your photo taken on the famous Kickback Couch - take the photo with you!"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61978828907013,
          29.583972700463683
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 12,
        "name": "Plates",
        "title": "Kickback License Plates",
        "location": "Grassy Area near Frost Bank",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61974269151686,
          29.583531849885443
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 14,
        "name": "Putt-Putt Golf",
        "title": "Putt-Putt Golf",
        "location": "Frost Bank - HUC ",
        "type": "activity",
        "description": "Learn about how Frost Bank to help you manage your money successfully while you play a fun round of putt-putt golf!"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.61985266208649,
          29.583487531466808
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 17,
        "name": "Spin the Wheel",
        "title": "Spin the Wheel &amp; Photo Booth",
        "location": "Campus Technology Store - Fountain Courtyard",
        "type": "activity",
        "description": "Spin the wheel for prizes, take some fun photos in the photo booth with the UTSA Campus Technology Store!"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62033545970917,
          29.583744111515347
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 18,
        "name": "Glitter",
        "title": "Glitter Tattoos",
        "location": "In front of Starbucks - UC Fountain Courtyard",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.6203408241272,
          29.583608823934682
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 19,
        "name": "T-Shirts!",
        "title": "T-Shirt Giveaway!",
        "location": "Under UC East Bridge - Paseo",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.6199626326561,
          29.583695128101926
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 20,
        "name": "Which Path?",
        "title": "Which Path?",
        "location": "Student Conduct - UC North 1st Floor Corridor",
        "type": "activity",
        "description": "Participate in the fun game of 'Which Path Will You Choose?' with UTSA Student Conduct &amp; Community Standards!"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.6199840903282,
          29.5840403440328
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 21,
        "name": "Airbrush",
        "title": "Airbrush Tattoos",
        "location": "In front of Starbucks - UC Fountain Courtyard",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62022012472153,
          29.583667137569282
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 25,
        "name": "Beanbag Toss",
        "title": "Beanbag Toss",
        "location": "UPS Store Table - UC North 1st Floor",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62012356519699,
          29.583935379854438
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 26,
        "name": "UC Giveaways!",
        "title": "UC Giveaways",
        "location": "UC Table - UC North 1st Floor",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.6200699210167,
          29.58385140843312
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 27,
        "name": "RA Check-In",
        "title": "UC Info - RA Check-In",
        "location": "UC Info Center - UC North 1st Floor",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62023890018463,
          29.58432024797477
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "id": 29,
        "name": "Rios Coupons",
        "title": "Rios Coupons &amp; Giveaways",
        "location": "UC Paseo - Rios Golden Cut Salons",
        "type": "activity",
        "description": ""
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          -98.62015038728714,
          29.58370679082155
        ]
      }
    }
  ]
}