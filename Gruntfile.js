'use strict';

module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-manifest-generator');

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		app: 'app',
		dist: 'dist',
    
    manifestGenerator:{
      test: {
        options:{
          //is cache all the html files in source files
          //{Boolean}
          //default:true
          includeHTML:true,
          //is cache all the images tags or inline style with background-images in the  html files in source files
          //{Boolean}
          //default:true
          includeHtmlImage:true,
          //is cache all the style files imported by the html
          //{Boolean}
          //default:true
          includeCSS:true,
          //is cache all the background-images in the css contents, which were used by the  html files
          //{Boolean}
          //default:true
          includeCssImage:true,
          //is cache all the js files in the html files
          //{Boolean}
          //default:true
          includeJS:true,
        },
        files: {
          //the task will scan all the source files, and generate 'test.manifest' file as the cache setting. 
          'test.manifest': ['dist/**.html', 'dist/**/*.css', 'dist/**/*.js', 'dist/**/*.png', 'app/images/*.png', 'app/tiles/**/*.png'],
        }
      }
    },

		sass: {
			dist: {
				options: {
					style: 'expanded', // expanded or nested or compact or compressed
					loadPath: '<%= app %>/bower_components/foundation/scss',
					compass: true,
					quiet: true
				},
				files: {
					'<%= app %>/css/app.css': '<%= app %>/scss/app.scss'
				}
			}
		},		

		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			all: [
				'Gruntfile.js',
				'<%= app %>/js/**/*.js'
			]
    },

		clean: {
			dist: {
				src: ['<%= dist %>/*']
			},
		},
		copy: {
			dist: {
				files: [{
					expand: true,
					cwd:'<%= app %>/',
					src: ['fonts/**', '**/*.html', '!**/*.scss', '!bower_components/**', 'geojson/**'],
					dest: '<%= dist %>/'
				}]
			},
		},

		imagemin: {
			target: {
				files: [{
					expand: true,
					cwd: '<%= app %>/images/',
					src: ['**/*.{jpg,gif,svg,jpeg,png}'],
					dest: '<%= dist %>/images/'
				}]
			}
		},

		uglify: {
			options: {
				preserveComments: 'some',
				mangle: false
			}
		},

		useminPrepare: {
			html: ['<%= app %>/index.html'],
			options: {
				dest: '<%= dist %>'
			}
		},

		usemin: {
			html: ['<%= dist %>/**/*.html', '!<%= app %>/bower_components/**'],
			css: ['<%= dist %>/css/**/*.css'],
			options: {
				dirs: ['<%= dist %>']
			}
		},

		watch: {
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['sass']
			},
			sass: {
				files: '<%= app %>/scss/**/*.scss',
				tasks: ['sass']
			},
			livereload: {
				files: ['<%= app %>/**/*.html', 
        '!<%= app %>/bower_components/**', 
        '<%= app %>/js/**/*.js', 
        '<%= app %>/css/**/*.css', 
        '<%= app %>/images/**/*.{jpg,gif,svg,jpeg,png}'],
				options: {
					livereload: true
				}
			}
		},

		connect: {
			app: {
				options: {
					port: 9000,
					base: '<%= app %>/',
					open: true,
					livereload: true,
					hostname: '0.0.0.0'
				}
			},
			dist: {
				options: {
					port: 9001,
					base: '<%= dist %>/',
					open: true,
					keepalive: true,
					livereload: false,
					hostname: '0.0.0.0'
				}
			}
		},

		wiredep: {
			target: {
				src: [
					'<%= app %>/**/*.html'
				],
				exclude: [
					'modernizr',
					'jquery-placeholder',
					'jquery.cookie',
					'foundation'
				]
			}
		}

	});

	
	grunt.registerTask('compile-sass', ['sass']);
	grunt.registerTask('bower-install', ['wiredep']);
	
	grunt.registerTask('default', ['compile-sass', 'bower-install', 'connect:app', 'watch']);
	grunt.registerTask('validate-js', ['jshint']);
	grunt.registerTask('server-dist', ['connect:dist']);

  grunt.registerTask('manifest', ['manifestGenerator:test']);
	
	grunt.registerTask('publish', ['compile-sass', 'clean:dist', 'validate-js', 'useminPrepare', 'copy:dist', 'concat', 'cssmin', 'uglify', 'usemin']);


  grunt.registerTask('mapbox', function() {
      var version = '2.0.0';
      var exec = require('child_process').exec;
      var fs = require('fs');
      //this.options().version;
      console.log('mapbox v' + version);

      var baseUrl = 'https://api.tiles.mapbox.com/mapbox.js/v'+version+'/';

      //async task
      var done = this.async();

      //reset mapbox folder in bower_components
      exec('rm -rf app/bower_components/mapbox.css');
      exec('rm -rf app/bower_components/mapbox.js');

      //grab latest mapbox js and css files through bower
      exec('bower install mapbox.js='+baseUrl+'mapbox.js --save', function(err, stdout) {
           console.log(stdout);
           exec('bower install mapbox.css='+baseUrl+'mapbox.css --save', function(err, stdout) {
               console.log(stdout);
               prepareStyles();
           });
      });

      //rename mapbox css to scss, move it to styles, replace image links with local versions (donwloaded later)
      function prepareStyles() {
          var scssPath = 'app/assets/styles/mapbox/mapbox.scss';
          fs.writeFileSync(scssPath, fs.readFileSync('app/bower_components/mapbox.css/index.css'));

          var scss = fs.readFileSync(scssPath).toString();

          scss = scss.replace(/url\(images\//g, 'url(../images/mapbox/');
          fs.writeFileSync(scssPath, scss);

          fetchImages();
      }

      //fetch mapbox image assets 
      function fetchImages() {
          process.chdir('app/assets/images/mapbox/');

          exec('curl -O '+baseUrl+'images/icons-000000.png', function() {
              exec('curl -O '+baseUrl+'images/icons-ffffff.png', function() {
                  exec('curl -O '+baseUrl+'images/icons-000000@2x.png', function() {
                      exec('curl -O '+baseUrl+'images/icons-ffffff@2x.png', function() {
                          done();
                      });
                  });
              });
          });
      }
    });
};
